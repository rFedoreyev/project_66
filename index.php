
<?php
function generate($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    print '

    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="admin.php" accept-charset="UTF-8" method="GET">
                    <input  style="margin-bottom:-130px;color:white;" type="submit" id="send" class="buttonform" value="Войти как администратор">
                </div>
        </div>
        </form>
';

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        if($_COOKIE['admin']=='1'){
            header('Location:login.php');
        }

        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('<br/> <a style="color:#e6b333;" href="login.php">войти</a> в аккаунт<br/> Логин : <strong>%s</strong>
        <br/> Пароль : <strong>%s</strong> ',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    $errors['abil'] = !empty($_COOKIE['abil_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div>Заполните имя корректно </div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div>Заполните почту </div>';
    }
    if ($errors['sex']) {
        setcookie('sex_error', '', 100000);
        $messages[] = '<div>Выберите пол </div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div>Введите биографию </div>';
    }
    if ($errors['check']) {
        setcookie('check_error', '', 100000);
        $messages[] = '<div>Подтвердите согласие </div>';
    }
    if ($errors['abil']) {
        setcookie('abil_error', '', 100000);
        $messages[] = '<div>Выберите сверхспособность </div>';
    }
    if ($errors['year']) {
        setcookie('year_error', '', 100000);
        $messages[] = '<div>Корректно введите дату рождения   </div>';
    }
    if ($errors['limb']) {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div>Выберите количество конечностей   </div>';
    }

    $user = 'u33679';
    $pass = '33454545';
    $db = new PDO('mysql:host=localhost;dbname=u33679', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    $values['abil_value'] = empty($_COOKIE['abil_value']) ? '' : $_COOKIE['abil_value'];
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
    $flag = 0;
    foreach($errors as $err){
        if($err==1)$flag=1;
    }

    if (!$flag&&!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            $login = $_SESSION['login'];
            $stmt = $db->prepare("SELECT id FROM users WHERE login = '$login'");
            $stmt->execute();
            $user_id='';
            while($row = $stmt->fetch())
            {
                $user_id=$row['id'];
            }

            $request = "SELECT name,email,year,sex,limb,bio,checkbox FROM form WHERE id = '$user_id'";
            $result = $db -> prepare($request);
            $result ->execute();

            $data = $result->fetch(PDO::FETCH_ASSOC);

        $values['fio'] = strip_tags($data['name']);
        $values['email'] = strip_tags($data['email']);
        $values['year_value'] = strip_tags($data['year']);
        $values['sex_value'] = strip_tags($data['sex']);
        $values['limb_value'] = $data['limb'];
        $values['bio_value'] = strip_tags($data['bio']);
        $values['check_value'] = strip_tags($data['checkbox']);



        $login_ses=$_SESSION['login'];
        $uid_ses=$_SESSION['uid'];

        echo '<head>
    <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link  href="style-form.css" rel="stylesheet"  media="all"/>
</head>
<div class="formname">
<label style="margin-top:50px;text-align: center">Вход выполнен : <br/><strong>Логин </strong>: '.$login_ses.' <br/><strong>Ваш id </strong>:  '.$uid_ses.' </label>
</div>';
            print '

    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input  style="color:white;" type="submit" id="send" class="buttonform" value="Выйти">
                    
        </form>
';
    }else{
       print '

<head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input  style="margin-bottom:-100px;color:white;" type="submit" id="send" class="buttonform" value="Авторизация">
        </form>
        ';
    }

    include('form.php');
} else {
    if (!isset($_COOKIE['admin'])) {
        setcookie('admin', '0');
    }

    $errors = FALSE;
    if (empty($_POST['fio']) || (preg_match("/^[a-z0-9_-]{2,20}$/i", $_POST['fio']))) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('sex_value', $_POST['radio2'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['textarea1'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('bio_value', $_POST['textarea1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('check_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
    }
    $kek = 0;
    $myselect = $_POST['select1'];
    for ($i = 0; $i < 5; $i++) {
        if ($myselect[$i] != NULL) {
            $kek = 1;
        }
    }
    if (!$kek) {
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('abil_value', $_POST['select1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio1'])) {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('limb_value', $_POST['radio1'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['birthyear']) || $_POST['birthyear'] < 1886 || $_POST['birthyear'] > 2021) {
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('year_value', $_POST['birthyear'], time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        header('Location: index.php');
        exit();
    } else {
        $user = 'u33679';
        $pass = '33454545';
        $db = new PDO('mysql:host=localhost;dbname=u33679', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
        setcookie('abil_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('limb_error', '', 100000);

        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])&&!empty($_SESSION['uid'])) {

            $fio = $_POST['fio'];
            $email = $_POST['email'];
            $birthyear = $_POST['birthyear'];
            $sex = $_POST['radio2'];
            $limb = $_POST['radio1'];
            $bio = $_POST['textarea1'];

            $login = $_SESSION['login'];
            $stmt = $db->prepare("SELECT id FROM users WHERE login = '$login'");
            $stmt->execute();
            $user_id='';
            while($row = $stmt->fetch())
            {
                $user_id=$row['id'];
            }

            $sql = "UPDATE form SET name='$fio',email='$email',year='$birthyear',sex='$sex',limb='$limb',bio='$bio' WHERE id='$user_id'";
            $stmt = $db->prepare($sql);
            $stmt->execute();



           
            $ability_data = ['god','wall','levity','kos','kol'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }
            $god=$ability_insert['god'];
            $wall=$ability_insert['wall'];
            $levity=$ability_insert['levity'];
            $kos=$ability_insert['kos'];
            $kol=$ability_insert['kol'];
            $sql = "UPDATE all_abilities SET ability_god='$god',ability_through_walls='$wall',ability_levity='$levity',ability_kostenko='$kos',ability_kolotiy='$kol' WHERE id='$user_id'";
            $stmt = $db->prepare($sql);
            $stmt->execute();

        }
        else {
            $login=generate(rand(1,25));

            $pass =generate(rand(1,25));

            $hash_pass=password_hash($pass, PASSWORD_DEFAULT);
            
            setcookie('login', $login);
            setcookie('pass', $pass);

            $user = 'u33679';
            $pass = '33454545';
            $db = new PDO('mysql:host=localhost;dbname=u33679', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


            $stmt = $db->prepare("INSERT INTO form (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
            $stmt->execute(array('fio' => $_POST['fio'], 'birthyear' => $_POST['birthyear'], 'email' => $_POST['email'], 'sex' => $_POST['radio2'], 'limb' => $_POST['radio1'], 'bio' => $_POST['textarea1'], 'checkbox' => $_POST['checkbox']));


            $ability_data = ['god','wall','levity','kos','kol'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }

            $stmt = $db->prepare("INSERT INTO all_abilities (ability_god, ability_through_walls,ability_levity,ability_kostenko,ability_kolotiy) VALUES (:god, :wal,:lev,:kos,:kol)");

            $stmt->bindParam(':god', $ability_insert['god']);
            $stmt->bindParam(':wal', $ability_insert['wall']);
            $stmt->bindParam(':lev', $ability_insert['levity']);
            $stmt->bindParam(':kos', $ability_insert['kos']);
            $stmt->bindParam(':kol', $ability_insert['kol']);
            $stmt->execute();

            $stmt = $db->prepare("INSERT INTO users (login, hash) VALUES (:login,:hash)");
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':hash', $hash_pass);
            $stmt->execute();


        }
        setcookie('save', '1');
        header('Location: index.php');
    }
}


